#!/usr/local/bin/ruby
require "cli/ui"

def gameslist()
  remain_in_cycle = true
  while remain_in_cycle
    system("clear")
    CLI::UI::Prompt.ask("What do you want to do?") do |handler|
      handler.option("Action Games") { |selection| action() }
      handler.option("Board Games") { |selection| board() }
      handler.option("Card Games") { |selection| card() }
      handler.option("Puzzle and Quiz Games") { |selection| puzzle() }
      handler.option("Roleplaying Games") { |selection| rpg() }
      handler.option("Screensavers") { |selection| screensavers() }
      handler.option("Simulation Games") { |selection| simulation() }
      handler.option("Other") { |selection| other() }
      handler.option("Go Back") { |selection| remain_in_cycle = false }
    end
  end
end

def action()
  CLI::UI::Prompt.ask("Choose an Action Game") do |handler|
    handler.option("hunt - a multi-player multi-terminal game") { |selection| system("hunt") }
    handler.option("worm - Play the growing worm game") { |selection| system("worm") }
    handler.option("nInvaders - Space invaders but ncurses") { |selection| system("nInvaders") }
    handler.option("Go Back") { |selection| }
  end
end

def board()
  CLI::UI::Prompt.ask("Choose a Board Game") do |handler|
    handler.option("backgammon - the game of backgammon") { |selection| system("backgammon") }
    handler.option("gomoku - game of 5 in a row") { |selection| system("gomoku") }
    handler.option("monop - Monopoly game") { |selection| system("monop") }
    handler.option("Go Back") { |selection| }
  end
end

def card()
  CLI::UI::Prompt.ask("Choose a Card Game") do |handler|
    handler.option("canfield - the solitaire card game canfield") { |selection| system("canfeild") }
    handler.option("cribbage - the card game cribbage") { |selection| system("cribbage") }
    handler.option("fish - play Go Fish") { |selection| system("fish") }
    handler.option("mille - play Mille Bornes") { |selection| system("mille") }
    handler.option("Go Back") { |selection| }
  end
end

def puzzle()
  CLI::UI::Prompt.ask("Choose a Puzzle/Quiz Game") do |handler|
    handler.option("robotfindskitten - you are a robot and you look for a kitten") { |selection| system("robotfindskitten") }
    handler.option("arithmetic - quiz on simple arithmetic") { |selection| system("arithmetic") }
    handler.option("greed - try to erase all characters on the screen") { |selection| system("greed") }
    handler.option("boggle - word search game") { |selection| system("boggle") }
    handler.option("hangman - Computer version of the game hangman") { |selection| system("hangman") }
    handler.option("robots - fight off villainous robots") { |selection| system("robots") }
    handler.option("snake - display chase game") { |selection| system("snake") }
    handler.option("tetris-bsd - the game of tetris") { |selection| system("tetris") }
    handler.option("quiz - random knowledge tests") { |selection| system("quiz") }
    handler.option("wump - hunt the wumpus in an underground cave") { |selection| system("wump") }
    handler.option("Go Back") { |selection| }
  end
end

def rpg()
  CLI::UI::Prompt.ask("Choose a Roleplaying Game") do |handler|
    handler.option("adventure - an exploration game") { |selection| system("adventure") }
    handler.option("battlestar - a tropical adventure game") { |selection| system("battlestar") }
    handler.option("phantasia - an interterminal fantasy game") { |selection| system("phantasia") }
    handler.option("Nethack - one of those roguelikes") { |selection| system("nethack") }
    handler.option("cataclysm - another roguelike but this one is HUGE") { |selection| system("cataclysm") }
    handler.option("Go Back") { |selection| }
  end
end

def screensavers()
  CLI::UI::Prompt.ask("Using all my bandwith, aren\' you?") do |handler|
    handler.option("rain - animated raindrops display") { |selection| system("rain") }
    handler.option("worms - animate worms on a display terminal") { |selection| system("worms") }
    handler.option("Go Back") { |selection| }
  end
end

def simulation()
  CLI::UI::Prompt.ask("Using all my bandwith, aren\' you?") do |handler|
    handler.option("atc - air traffic controller game") { |selection| system("atc") }
    handler.option("sail - multi-user wooden ships and iron men") { |selection| system("sail") }
    handler.option("trek - trekkie game") { |selection| system("trek") }
    handler.option("moon-buggy - drive a moon buggy, try not to crash") { |selection| system("moon-buggy") }
    handler.option("Go Back") { |selection| }
  end
end

def other()
  CLI::UI::Prompt.ask("These aren\'t exactly games but they might be fun") do |handler|
    handler.option("banner - print large banner on printer") { |selection| system("banner") }
    handler.option("bcd - reformat input as punch cards, paper tape or morse code") { |selection| system("bcd") }
    handler.option("morse - reformat input as punch cards, paper tape or morse code") { |selection| system("morse") }
    handler.option("number - convert Arabic numerals to English") { |selection| system("number") }
    handler.option("pig - eformatray inputway asway Igpay Atinlay") { |selection| system("pig") }
    handler.option("ppt - reformat input as punch cards, paper tape or morse code") { |selection| system("ppt") }
    handler.option("random - random lines from a file or random numbers") { |selection| system("random") }
    handler.option("rot13 - rot13 encrypt/decrypt") { |selection| system("rot13") }
    handler.option("caesar - decrypt caesar cyphers") { |selection| system("caesar") }
    handler.option("pom - display the phase of the moon") { |selection| system("pom") }
    handler.option("primes - generate primes") { |selection| system("primes") }
    handler.option("cfscores - show scores for canfield") { |selection| system("cfscores") }
    handler.option("huntd - hunt daemon, back-end for hunt game") { |selection| system("huntd") }
    handler.option("snscore - show scores for snake") { |selection| system("snscore") }
    handler.option("teachgammon - learn to play backgammon") { |selection| system("teachgammon") }
    handler.option("wargames - shall we play a game?") { |selection| system("wargames") }
    handler.option("wtf - translates acronyms for you") { |selection| system("wtf") }
    handler.option("Go Back") { |selection| }
  end
end
