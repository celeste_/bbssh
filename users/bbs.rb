#!/usr/local/bin/ruby
require "cli/ui"
require "colorize"
require "lolize"
require_relative "games.rb"
require_relative "mail.rb"
require_relative "corkboard.rb"

def main()
  CLI::UI::StdoutRouter.enable
  menu()
end

def welcome_panel()
  CLI::UI::Frame.open("News") do
    motd = File.read("/usr/local/etc/bbssh/news.txt")
    art = File.read("/usr/local/etc/bbssh/art.txt")
    colorizer = Lolize::Colorizer.new
    colorizer.write "#{motd}"
    colorizer.write "#{art}"
    #CLI::UI::Frame.close('bbssh.club')
  end
end

def exitmenu()
  puts "You have been dumped into a shell".colorize(:blue)
  puts "to start the menu again run /opt/bbssh/users/bbs.rb".colorize(:blue)

  exit(0)
end

def menu()
  system("clear")
  welcome_panel()
  CLI::UI::Prompt.ask("What do you want to do?") do |handler|
    handler.option("Shell") { |selection| system("/bin/sh") }
    handler.option("Games") { |selection| gameslist() }
    handler.option("Mail") { |selection| mail() }
    handler.option("Public Corkboard") { |selection| corkboard() }
    handler.option("Close Menu") { |selection| exitmenu() }
  end
  menu()
end

main()
