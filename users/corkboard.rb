#!/usr/local/bin/ruby
require "cli/ui"
require "colorize"
require "json"

# Json format to add a post
# {
#   "user": username,
#   "password": entered_password,
#   "announcement": a nested "Announcement" Object,(see Announcement class for more info)
# }

module CorkBoard
  class Announcement
    # {
    #   "date": "yyyy-mm-dd", <- this can be ignored when posting, just let the db auto-insert a Date.
    #   "title": "",
    #   "text": "",
    #   "author_name": "",
    #   "author_mail": "",
    #   "category": ""
    # }
    def initialize(date, title, text, author_name, author_mail)
      @date = date
      @title = title
      @text = text
      @author_name = author_name
      @author_mail = author_mail
    end

    def print_announcement()
      CLI::UI::Frame.open(@title + " - " + @date) do
        puts(@text)
        puts("\n\n")
        puts("Author: " + @author_name + "\nContact me at " + @author_mail)
      end
    end
  end

  class Category
    @list_of_announcements = []

    def initialize(cat_name, list_of_announcements)
      @list_of_announcements = list_of_announcements
      @cat_name = cat_name
    end

    def add_element(new_announcement)
      @list_of_announcements.push(new_announcement)
    end

    def get_name()
      return @cat_name
    end

    def print_category()
      CLI::UI::Frame.open("Category: " + @cat_name, color: :green) do
        @list_of_announcements.each do |e|
          e.print_announcement()
        end
      end
    end
  end

  class Manager
    def initialize()
      @data = fetch_existing_categories_and_announcements()
      @current_category = @data[0].get_name() #set this to the server
    end

    def get_categories_list()
      categories_strings = []
      @data.each do |c|
        categories_strings.push(c.get_name())
      end
      return categories_strings
    end

    def print_category(category_name)
      @data.each do |c|
        if category_name == c.get_name()
          c.print_category()
        end
      end
    end

    def print()
      print_category(@current_category)
    end

    def set_category(category_name)
      @current_category = category_name
    end

    def force_update()
      # forces a fetch of data from the server
      @data = fetch_existing_categories_and_announcements()
    end

    def fetch_existing_categories_and_announcements()
      # asks the server a list of existing categories and Announcements
      # it creates an array of Category objects with linked Announcement objects
      return [
               CorkBoard::Category.new("Bbssh's Latest News", [
                 CorkBoard::Announcement.new("2020-08-26", "Repo change", "Our WWW folder has moved to a new repository! Go look at https://gitlab.com/beegrrl/bbssh-www/", "Example author", "example@bbssh.club"),
                 CorkBoard::Announcement.new("2020-08-22", "Site revamp", "Check out our new mobile-friendly homepage at https://bbssh.club", "Example author", "example@bbssh.club"),
               ]),
               CorkBoard::Category.new("Games", []),
               CorkBoard::Category.new("Help needed", []),
               CorkBoard::Category.new("Miscellaneous", []),
             ]
    end
  end
end

def heading()
  system("clear")
  CLI::UI::Frame.open("Corkboard", color: :green) do
    puts("Welcome to the corkboard section\nThis is a public place all of our users")
  end
end

def corkboard()
  man_obj = CorkBoard::Manager.new()
  remain_in_cycle = true
  while remain_in_cycle
    heading()
    man_obj.print()
    puts "\n"
    CLI::UI::Prompt.ask("You can select a category, list post-its and add yours") do |handler|
      man_obj.get_categories_list().each do |c|
        handler.option("Category: " + c) {
          |selection|
          man_obj.set_category(c)
        }
      end
      handler.option("Add a post-it") { |selection| new_post_menu() }
      handler.option("Back To Main Menu") { remain_in_cycle = false }
    end
  end
end

def new_post_menu()
  heading()
  CLI::UI::Frame.open("Add a new post-it", color: :red) do
    puts("This section is still in development and not ready yet.\nCome back later! :)")
  end
  puts("Press enter to go back")
  gets()
end
