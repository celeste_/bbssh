#!/usr/local/bin/ruby
print "Enter the Username: "
username = gets.chomp

# set up zvols
system("zfs create zroot/usr/home/#{username}")
system("zfs set quota=500M zroot/usr/home/#{username}")


# add new user
system("adduser")
puts "Paste your ssh pubkey here (or press enter to skip)"
sshpubkey = gets.chomp
File.write("/usr/home/#{username}", "#{sshpubkey}", mode: 'a')

# set up apache homedirs
system("chmod o-rwx /home/#{username}")
system("chmod o+x /home/#{username}")
system("mkdir /home/#{username}/public_html")
system("chown #{username}:wheel /home/#{username}/public_html")
system("chmod o+x /home/#{username}/public_html")
system("touch /home/#{username}/public_html/index.html")
system("echo 'hello world!' >> /home/#{username}/public_html/index.html")


# adds user to the "users" login class
# see /etc/rctl.conf for resource limiting
system("pw usermod #{username} -L users")
