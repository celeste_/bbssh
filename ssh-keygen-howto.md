# Generating an SSH key

```sh
# to generate an ssh key:
ssh-keygen

# which will output something like this. Fill in prompts accordingly.
# The values in paranthesis are default
Generating public/private rsa key pair.
Enter file in which to save the key (/home/$user/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/$user/.ssh/id_rsa.
Your public key has been saved in /home/$user/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:A-big-long-string-of-random-characters $user@localhost
The key's randomart image is:
+---[RSA 2048]----+
|      _____      |
|    .'     '.    |
|   /  o   o  \   |
|  |           |  |
|  |  \     /  |  |
|   \  '---'  /   |
|    '._____.'    |
|                 |
|                 |
+----[SHA256]-----+

# the public key can be copy-pasted after running:
cat ~/.ssh/id_rsa.pub
```

