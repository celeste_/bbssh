# mail(1) -> or "wait, this is actually ed???" 
After some time struggling to understand mail(1), I've decided to write a "all you need to know in 15 minutes or less" guide. 

If you've never used a line editor (like ed or vim's line mode) it might be a bit confusing. Mail is sparan. Mail is not mutt or alpine. But you're in luck -- this crash course guide will help. 

# How do I read mail?
upon login, you might see "you have mail". In order to read it run
```sh
mail
```
now you ask yourself "what the hell is going on?" Each message is indexed by a number. Typing the message number and pressing return will not only **select the message** but also **print the message**. Other ways to print the message include:
```sh
p $message_number
t $message_number
```

# I want to open mail(1) but it keeps saying "no mail for $user"

```sh
mail -f /var/mail/$user
```

# How do I reply to mail?
Select the message you want to reply to by typing it's number, then return. To 'reply all' type 'r', to reply to only the sender type 'R', then press return. You are now in a text buffer of sorts. Type out whatever you want. When you are done press enter then Control+d to send the message. If you want to abort you can press Control+c twice

# How do I send mail?
Sending mail is easy! To send mail you will type 'm' followed by a list of users you want to send it to then press enter. Example:
```sh
m foo@localhost bar@localhost baz@localhost
```
Now you'll see a subject line. Type the subject then press enter. You're now in the text buffer. Type all the things you want to say. When you're ready to send press enter then Control+d to send. If you want to abort you can press Control+c twice

# How do I delete mail?

```sh
d $message_number
```

# how do I undelete mail?

```sh
u $message_number
```

# how do I save mail?

```sh
s $message_number saved-message.txt
```

# how do I exit? Control+c isn't working?
type Control+d or q

