# shared hosting using apache
  - [x] ssl
  - [x] php
  - [ ] userdirs
# sign up page
  - probably need sqlite
  - need 
	+ [ ] email
	+ [ ] ssh pubkey
	+ [ ] username (and check if it's already taken
	+ [ ] index of users

# security
  - [x] pf
  - [ ] /etc/login.conf
  	- limit max procs and set default niceness to 10ish 
  - [ ] figure out login groups 	
  	+ /etc/login.conf *only* matches to the loigin group
	+ something like "pw usermod $user-to-modify -L $login-group"
